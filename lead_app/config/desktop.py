from frappe import _

def get_data():
	return [
		{
			"module_name": "Lead App",
			"type": "module",
			"label": _("Lead App")
		}
	]
