frappe.ui.form.on("Lead", {
    refresh: function (frm) {
        frm.add_custom_button("Hello", () => {
            frappe.msgprint('Hello World!!');
        });

        // prompt for single value of type Data and Set title
        frm.add_custom_button(__("Fetch Name"), () => {
            frappe.prompt({
                label: 'First Name',
                fieldname: 'first_name',
                fieldtype: 'Data'
            }, (value) => frm.set_value('first_name', value.first_name), 'Enter First Name')
        });
    },

    validate: function (frm) {
        var pan_number = frm.doc.custom_pan_number;

        // Define a regular expression pattern for PAN validation
        var pan_pattern = /^[A-Z]{5}[0-9]{4}[A-Z]{1}$/;

        if (pan_number && !pan_pattern.test(pan_number)) {
            frappe.msgprint(__("PAN Number should have the format: ABCDE1234F"));
            frappe.validated = false;
        }
    },

    custom_contact: function (frm) {
        frappe.call({
            method: "frappe.client.get",
            args: {
                doctype: "Lead Contact",
                name: frm.doc.custom_contact
            },
            callback: (r) => {
                if (r.message) {
                    frm.set_value('email_id', r.message.email);
                    frm.set_value('website', r.message.website);
                    frm.set_value('mobile_no', r.message.mobile_no);
                    frm.set_value('whatsapp_no', r.message.whatsapp);
                    frm.set_value('phone', r.message.phone);
                }
            }
        });
    }
})