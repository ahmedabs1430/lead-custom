import frappe


def after_insert(doc, method):
    # Create a new "Lead Details" document
    lead_doc = frappe.new_doc("Lead Details")

    lead_doc.first_name = doc.first_name
    lead_doc.middle_name = doc.middle_name
    lead_doc.last_name = doc.last_name
    lead_doc.gender = doc.gender
    lead_doc.lead = doc.name

    lead_doc.insert()


def on_update(doc, method):
    # Update the existing "Lead Details" document if it exists
    lead_details = frappe.get_doc("Lead Details", {"lead": doc.name})

    lead_details.first_name = doc.first_name
    lead_details.middle_name = doc.middle_name
    lead_details.last_name = doc.last_name
    lead_details.gender = doc.gender

    lead_details.save()
